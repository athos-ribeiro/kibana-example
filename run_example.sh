#!/bin/bash

sudo sysctl -w vm.max_map_count=262144 > /dev/null 2>&1
docker-compose build > /dev/null 2>&1
docker-compose down > /dev/null 2>&1
docker-compose up -d > /dev/null 2>&1
#docker-compose logs
echo you can access kibana on port 5601
docker build ./webapp -t dockerelkstack_webapp > /dev/null 2>&1
docker run --network dockerelkstack_logging --link redis:redis -p 80:80 -d --name webapp dockerelkstack_webapp > /dev/null 2>&1
#docker logs webapp
echo you can access the example webapp on YOUR port 80
